#Challenge de Web
#Javascript - Authentification(Web Exploit)
(https://www.root-me.org/fr/Challenges/Web-Client/Javascript-Authentification --14 Avril 2022)-- Très Facile
#Solutions
Solution(s) externe(s):4
Propre Solution:1
Inspecter le code source. Réperer et ouvrir le fichier Javascript. Le mot de passe est facilement répérable.

/* <![CDATA[ */

function Login(){
	var pseudo=document.login.pseudo.value;
	var username=pseudo.toLowerCase();
	var password=document.login.password.value;
	password=password.toLowerCase();
	if (pseudo=="4dm1n" && password=="sh.org") {
	    alert("Password accepté, vous pouvez valider le challenge avec ce mot de passe.\nYou an validate the challenge using this password.");
	} else { 
	    alert("Mauvais mot de passe / wrong password"); 
	}
}
/* ]]> */ 
