#Challenge de Web
#Javascript - Obfuscation 2(Web Exploit)
(https://www.root-me.org/fr/Challenges/Web-Client/Javascript-Obfuscation-2 --14 Avril 2022)-- Très Facile
#Solutions
Solution(s) externe(s):0
Propre Solution:1
Il faut affcher le code source
Executer premièrement le bout de code suivant pour la première déobfuscation
var pass=unescape("%2528104%252C68%252C117%252C102%252C106%252C100%252C107%252C105%252C49%252C53%252C54%2529%22%29")
on obtient %28104%2C68%2C117%2C102%2C106%2C100%2C107%2C105%2C49%2C53%2C54%29. Il faut à nouveau déobfusquer ce bout de code afin d'obtenir ceci (104,68,117,102,106,100,107,105,49,53,54)
Pour avoir le mot de passe il suffit de placer ce code ascii dans la méthode fromCharCode.
