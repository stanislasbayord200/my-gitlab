#Challenge de Web
#HTTP-POST (Web Exploit)
(https://www.root-me.org/fr/Challenges/Web-Serveur/HTTP-POST -- 15 Avril 2022)--Facile
#Solutions
Solution(s) externe(s):0
Propre Solution:1
Il s'agit ici de modifier le nombre aléatoire pour qu'il surpasse le nombre donné. Pour cela il faut inspecter le code. Modifier le nombre qui multiplie le nombre aléatoire au niveau de l'évènement onsubmit afin que ce nombre soit supérieur à 999999 
