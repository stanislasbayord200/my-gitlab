#Challenge de Web
#Some Assembly Required 1(Web Exploit)
(https://play.picoctf.org/practice/challenge/152?category=1&page=14-- Avril 2022)--Moyen
#Solutions
Solution(s) externe(s):0
Propre Solution:1
Il faut télécharger le fichier de web assembleur avec la commande :wget http://mercury.picoctf.net:1896/JIFxzHyW8W -q -O script2.wasm
Ensuite générer un fichier en clair avec celle-ci: wasm2wat --generate-names script2.wasm > script2.wat
En ouvrant le dernier fichier on retrouve le flag
