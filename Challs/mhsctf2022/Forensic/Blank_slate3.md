#Challenge de Forensic
#Blank Slate 3(Forensic)
(https://mhsctf2022.ctfd.io/challenges#Blank%20Slate%203-23 --25 Février 2022)--Facile
#Solutions
Solution(s) externe(s):0
Propre solution:1
Ici on a une image qui semble vide. Il faut importer l'image sur cyberchef. Aller dans la section multimedia et choisir normalise image. Le flag apparaitra sur l'image.
