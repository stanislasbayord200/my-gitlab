#Challenge de Forensic
#Blatant Corruption(Forensic)
(https://mhsctf2022.ctfd.io/challenges#Blatant%20Corruption-34 --25 Février 2022)--Moyen
#Solutions
Solution(s) externe(s):0
Propre Solution:1
Il s'agissait d'utiliser une image pour trouver le flag. L'image n'était pas complète. Il faut l'ouvrir avec un éditeur hexadecimal et ajouter '.PNG' au début conformément aux images PNG. Ensuite ouvrez l'image avec un visionneur d'image ,le flag devrait s'afficher.
