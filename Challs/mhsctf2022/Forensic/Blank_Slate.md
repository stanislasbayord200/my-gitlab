#Challenge de Forensic
#Blank Slate(Forensic)
(https://mhsctf2022.ctfd.io/challenges --21 Février 2022)--Facile
#Solutions
Solution(s) externe(s):0
Propre Solution(s):1
Il s'agit de trouver le flag grâce à une photo. Pour cela il faut ouvir la photo avec un éditeur hexdécimal comme Ghex sur linux.En regardant dans la partie non hexadécimal on retrouve le flag.
