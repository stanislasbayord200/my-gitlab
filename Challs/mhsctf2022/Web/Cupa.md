#Challenge de Web
#Cuppa Joe(Web Exploit)
(https://mhsctf2022.ctfd.io/challenges#Cuppa%20Joe-25 --21 Février 2022)--Facile
#Solutions
Solution(s) externe(s):0
Propre Solution:1
On a un site avec un formulaire. Lorsqu'on remplit le formulaire et on envoi on a pas le flag. Il faut inspecter le code. Réperer la ligne de la balise ouvrante <form>. Ensuite il faut modifier le fichier de réception des données du formulaire. par flag.php Pour cela il faut faire un clique droit sur cette ligne et choisir "Edit as HTML". Retourner ensuite sur le formulaire et envoyer ce que vous voulez. Le flag apparaitra.
