#Challenge de type General
#Where the Wild Cards Are(General)
(https://mhsctf2022.ctfd.io/challenges#Where%20the%20Wild%20Cards%20Are-6 --22 Février 2022)--Moyen
#Solutions
Solution(s) externe(s):0
Propre Solution:1
On a un fichier avec du texte et une expression regulière. L'objectif est de se baser sur cette expression pour retrouver la flag dans le fichier. Il faut ouvrir le fichier dans un éditeur (Pluma par exemple). Ensuite il faut effectuer une recherche avec l'expression régulière comme élément recherché. N'oubliez pas de cocher l'option <faire correspondre à une expression reulière >. Notez les caractères obtenus et effacer les doublons intrus pour avoir le flag.
